<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use frontend\widgets\NewBigBlock;

$this->title = 'Главная страница';
?>
<div class="site-index">
    <h1>Последние новости</h1>

    <? foreach($items as $item):?>
        <?= NewBigBlock::widget([
            'item' => $item
        ]) ?>
    <? endforeach;?>
</div>
