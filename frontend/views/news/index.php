<?php
/* @var $this yii\web\View */

use frontend\widgets\NewBigBlock;
use yii\widgets\LinkPager;

$this->title = 'Новости';
$this->params['breadcrumbs'][] = [
    'label' => 'Новости',
    'url'   => '/news/index.html'
];

if ($search){
    $this->title = 'Новости согласно запросу - '.$search;
    $this->params['breadcrumbs'][] = $this->title;
}

?>
<h1>
    Новости
    <? if ($search):?>
        согласно запросу &laquo;<?= $search ?>&raquo;
    <? endif; ?>
    </h1>

<? foreach($items as $item):?>
    <?= NewBigBlock::widget([
        'item' => $item
    ]) ?>
<? endforeach; ?>


<?= LinkPager::widget([
    'pagination' => $pages,
]);?>