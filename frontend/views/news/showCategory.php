<?php
/**
 * Created by PhpStorm.
 * User: info_000
 * Date: 08.03.2016
 * Time: 22:17
 */

use frontend\widgets\NewBigBlock;
use yii\widgets\LinkPager;

$this->title = 'Категория '.mb_strtolower($categoryName);
$this->params['breadcrumbs'][] = [
    'label' => 'Новости',
    'url'   => '/news/index.html'
];
$this->params['breadcrumbs'][] = $this->title;

?>


<h1>Категория <?= mb_strtolower($categoryName) ?></h1>

<? if($items): ?>
    <? foreach($items as $item):?>
        <?= NewBigBlock::widget([
            'item' => $item
        ]) ?>
    <? endforeach;?>
<? else: ?>
    <div>Согласно выбранной категории новости отсутствуют</div>
<? endif; ?>


<?= LinkPager::widget([
    'pagination' => $pages,
]);?>