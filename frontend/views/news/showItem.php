<?php
/**
 * Created by PhpStorm.
 * User: info_000
 * Date: 08.03.2016
 * Time: 1:26
 */


use yii\helpers\Url;
use yii\helpers\Html;

$this->title = 'Новости ::  '.$item->name;
$this->params['breadcrumbs'][] = [
    'label' => 'Новости',
    'url'   => '/news/index.html'
];

$this->params['breadcrumbs'][] = $item->name;
?>

<h1><?= $item->name ?></h1>



<? if ($item['image']): ?>
    <div class="newsShowItemImg"><?= Html::img('/uploads/'.$item['image']) ?></div>
<? endif; ?>

<div class="newsShowItemDate">Дата: <?=Yii::$app->formatter->asDatetime($item['date'], 'yyyy-MM-dd H:i')?></div>

<div class="newsShowItemText">
    <?= nl2br($item->text) ?>
</div>


<div class="newsShowItemBacklink">
    <a href="<?= Url::to('/news/index.html') ?>">  	&larr; Перейти к спику новостей</a>
</div>

<div class="newsShowItemClear"></div>