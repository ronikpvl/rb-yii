<?php

namespace frontend\controllers;

use yii\web\Controller;
use frontend\models\News;
use frontend\models\NewsCategory;
use yii\data\Pagination;


class NewsController extends Controller
{
    public function actionIndex()
    {
        $search = \Yii::$app->request->get('search') ? \Yii::$app->request->get('search') : null;

        $query = News::find()
            ->select(['id', 'name', 'anons', 'date', 'image'])
            ->orderBy('id DESC');

        if ($search){
            $query->andWhere(
                'name LIKE "%'.$search.'%"
                 OR text LIKE "%'.$search.'%"
                 OR anons LIKE "%'.$search.'%"'
            );
        }

        $pages = new Pagination([
            'totalCount' => $query->count(),
            'pageSize' => 6
        ]);

        $data = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        return $this->render('index', [
            'items' => $data,
            'pages' => $pages,
            'search' => $search
        ]);
    }


    public function actionShowCategory($id)
    {
        $categoryName = NewsCategory::findOne($id);
        $categoryName = $categoryName['name'];

        $query = News::find()
            ->select(['id', 'name', 'anons', 'date', 'image'])
            ->where(['category_id' => $id])
            ->orderBy('id DESC');

        $pages = new Pagination([
            'totalCount' => $query->count(),
            'pageSize' => 6
        ]);

        $data = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        return $this->render('showCategory', [
            'items'        => $data,
            'pages'        => $pages,
            'categoryName' => $categoryName
        ]);
    }

    public function actionShowItem($id)
    {
        $item = News::find()
            ->select(['name', 'anons', 'text', 'date', 'image'])
            ->where(['id' => $id])
            ->one();

        return $this->render('showItem', [
            'item'=> $item
        ]);
    }


}
