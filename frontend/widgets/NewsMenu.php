<?php
/**
 * Created by PhpStorm.
 * User: info_000
 * Date: 01.03.2016
 * Time: 22:57
 */

namespace frontend\widgets;

use yii\base\Widget;
use yii\helpers\ArrayHelper;
use frontend\models\NewsCategory;


class NewsMenu extends Widget
{
    public $items;

    public function run()
    {
        $this->items = NewsCategory::find()->orderBy('name ASC')->all();
        $this->items = ArrayHelper::toArray($this->items,[
            'app\models\Post' => [
                'id',
                'name',
            ]
        ]);

        return $this->render('NewsMenu', [
            'items' => $this->items
        ]);
    }
}