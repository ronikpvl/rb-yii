<?php
/**
 * Created by PhpStorm.
 * User: info_000
 * Date: 02.03.2016
 * Time: 0:26
 */

namespace frontend\widgets;

use yii\base\Widget;


class NewBigBlock extends Widget
{
    public $item;

    public function run()
    {
        return $this->render('newBigBlock', [
            'item' => $this->item,
        ]);
    }
}