<?php
/**
 * Created by PhpStorm.
 * User: info_000
 * Date: 01.03.2016
 * Time: 22:59
 */

use yii\helpers\Html;
use yii\helpers\Url;
?>


<div id="leftMenuContainer">
    <? foreach($items as $item):?>
        <div class="leftMenuItem">
            <a href="<?= Url::to(['/news/show-category', 'id' => $item['id']]) ?>" title="Категория <?= $item['name'] ?>"><?= $item['name'] ?></a>
        </div>
    <? endforeach ?>
</div>