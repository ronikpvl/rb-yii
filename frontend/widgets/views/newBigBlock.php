<?php
/**
 * Created by PhpStorm.
 * User: info_000
 * Date: 08.03.2016
 * Time: 0:56
 */

use yii\helpers\Url;
use yii\helpers\Html;
?>

<div class="newBigBlockContainer">

    <div class="newBigBlockTitle"><a href="<?= Url::to(['news/show-item', 'id' => $item['id']]) ?>"><?=$item['name']?></a></div>
    <? if ($item['image']): ?>
         <div class="newBigBlockImg"><?= Html::img('/uploads/'.$item['image']) ?></div>
    <? endif; ?>
    <div class="newBigBlockAnons"><?=$item['anons']?></div>
    <div class="newBigBlockDate">Дата: <?=Yii::$app->formatter->asDatetime($item['date'], 'yyyy-MM-dd H:i')?></div>
    <div class="newBigBlocClear"></div>
</div>
