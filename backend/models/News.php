<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "news".
 *
 * @property integer $id
 * @property string $name
 * @property integer $category_id
 * @property string $anons
 * @property string $text
 *
 * @property NewsCategory $category
 */
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

class News extends ActiveRecord
{
    public $imageFile;
    public $deleteImage;




    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['date'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['date'],
                ],
                'value' => function() { return date('U'); },
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'category_id', 'anons'], 'required'],
            [['category_id'], 'integer'],
            [['text'], 'string'],
            [['name', 'anons', 'image'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'category_id' => 'Категория',
            'anons' => 'Анонс',
            'text' => 'Текст',
            'categoryName' => Yii::t('app', 'Категория'),
            'image' => 'Картинка',
            'deleteImage' => '- удалить картинку'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(NewsCategory::className(), ['id' => 'category_id']);
    }

    public function getCategoryName()
    {
        $param =  $this->category;

        return $param ? $param->name : '';
    }
}
