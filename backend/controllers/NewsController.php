<?php

namespace backend\controllers;

use backend\models\NewsCategory;
use Yii;
use backend\models\News;
use backend\models\NewsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;
use yii\filters\AccessControl;

/**
 * NewsController implements the CRUD actions for News model.
 */
class NewsController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'view', 'create', 'update', 'delete'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view', 'create', 'update', 'delete'],
                        'roles' => ['@'],
                    ],
                ],
            ],

            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all News models.
     * @return mixed
     */
    public function actionIndex()
    {

        $searchModel = new News();
        $dataProvider = new ActiveDataProvider([
            'query' => News::find()->orderBy('id DESC'),
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel
        ]);
    }

    /**
     * Displays a single News model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new News model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model         = new News();

        $categoryItems = NewsCategory::find()->all();
        $categoryItems = ArrayHelper::map($categoryItems,'id','name');
        $params        = ['prompt' => 'Выберите категорию новостей'];

        if ($model->load(Yii::$app->request->post())) {

            $model->save();

            $imageFile = UploadedFile::getInstance($model, 'imageFile');

            $fileDir  = 'uploads/';

            if ($imageFile) {
                $fileName = 'news_' . Yii::$app->db->getLastInsertID() . '.' . $imageFile->extension;
                $imageFile->saveAs($fileDir . $fileName);
                $model->image = $fileName;

                $model->save();
            }

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model'         => $model,
            'categoryItems' => $categoryItems,
            'params'        => $params
        ]);
    }

    /**
     * Updates an existing News model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $categoryItems = NewsCategory::find()->all();
        $categoryItems = ArrayHelper::map($categoryItems,'id','name');
        $params        = ['prompt' => 'Выберите категорию новостей'];


        if ($model->load(Yii::$app->request->post())) {
            $imageFile = UploadedFile::getInstance($model, 'imageFile');

            $fileDir  = 'uploads/';

            if ($imageFile) {
                $fileName = 'news_' . $model->id . '.' . $imageFile->extension;
                $imageFile->saveAs($fileDir . $fileName);
                $model->image = $fileName;
            }

            $request = Yii::$app->request->post();

            if ($request['News']['deleteImage']){
                unlink($fileDir . $model->image);
                $model->image = null;
            }

            if ($model->save()){
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model'         => $model,
            'categoryItems' => $categoryItems,
            'params'        => $params
        ]);
    }

    /**
     * Deletes an existing News model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the News model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return News the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = News::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
