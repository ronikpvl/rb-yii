<?php

use yii\db\Migration;
use yii\db\Schema;

class m160309_172641_alter_news extends Migration
{
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->addColumn('news', 'date', Schema::TYPE_INTEGER.' NOT NULL');
    }

    public function safeDown()
    {
        $this->dropColumn('news', 'date');
    }

}
