<?php

use yii\db\Migration;
use yii\db\Schema;

class m160224_160041_news extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('news_category', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING.' NOT NULL'
        ], $tableOptions);

        $this->createTable('news', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING.' NOT NULL',
            'category_id' => Schema::TYPE_INTEGER.' NOT NULL',
            'anons' =>Schema::TYPE_STRING.' NOT NULL',
            'text'=>Schema::TYPE_TEXT
        ], $tableOptions);

        $this->addForeignKey('category_id', 'news', 'category_id', 'news_category', 'id');
    }

    public function safeDown()
    {
        $this->dropTable('news');
        $this->dropTable('news_category');
    }
}
