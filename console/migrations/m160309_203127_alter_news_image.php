<?php

use yii\db\Migration;
use yii\db\Schema;

class m160309_203127_alter_news_image extends Migration
{
    public function up()
    {
        $this->addColumn('news', 'image', Schema::TYPE_STRING);
    }

    public function down()
    {
        $this->dropColumn('news', 'image');

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
