# Требования для установки/работы приложения: # 

* Apache 2.4 (без nginx)
* PHP >= 5.5.0
* Mysql 5.6
* Composer


# Установка приложения Yii2 Advanced #

### Разворачиваем приложение из git репозитория: ### 


```
#!git

git clone https://ronikpvl@bitbucket.org/ronikpvl/rb-yii.git
```



### Для того чтобы исключить возможные проблемы установки приложения Yii2 Advanced нужно выполнить(описано в офф доках): ### 

```
#!composer

php composer.phar global require "fxp/composer-asset-plugin:~1.1.1"

php composer.phar update yiisoft/yii2 yiisoft/yii2-composer bower-asset/jquery.inputmask

php composer.phar self-update

```


### Для установки всех необходимых пакетов выполнить команду ###
```
#!composer

php composer.phar update
```
 

### Для дальнейшей инициализации приложения в корне выполнить команду ### 


```
#!php

init
```


### Подключение к БД. ###
Необходимо создать какую-либо базу данных для приложения. После создания базы, прописать все данные для подключения к бд. 
Конфигурационный файл базы данных лежит в следующем скрипте: /common/config/main-local.php


### Далее необходимо применить все миграции, для этого вводим команду ### 


```
#!php

yii migrate
```


----------------------------------------

У приложения есть две части frontend и backend. Чтобы перейти в backend часть нужно ввести следующий адрес http://host_name.local/admin

Также важен правильно настроеyный apache так как очень много чего завязано на .htaccess

----------------------------------------

**[Frontend Screen](https://i.gyazo.com/d8db03bde3b17c7b657fff01123dcd96.png)

[Backend Screen](https://i.gyazo.com/ecaf62744cca468ef1d1a0ab86e6baf0.png)**


----------------------------------------



Yii 2 Advanced Project Template
===============================

Yii 2 Advanced Project Template is a skeleton [Yii 2](http://www.yiiframework.com/) application best for
developing complex Web applications with multiple tiers.

The template includes three tiers: front end, back end, and console, each of which
is a separate Yii application.

The template is designed to work in a team development environment. It supports
deploying the application in different environments.

Documentation is at [docs/guide/README.md](docs/guide/README.md).

[![Latest Stable Version](https://poser.pugx.org/yiisoft/yii2-app-advanced/v/stable.png)](https://packagist.org/packages/yiisoft/yii2-app-advanced)
[![Total Downloads](https://poser.pugx.org/yiisoft/yii2-app-advanced/downloads.png)](https://packagist.org/packages/yiisoft/yii2-app-advanced)
[![Build Status](https://travis-ci.org/yiisoft/yii2-app-advanced.svg?branch=master)](https://travis-ci.org/yiisoft/yii2-app-advanced)

DIRECTORY STRUCTURE
-------------------

```
common
    config/              contains shared configurations
    mail/                contains view files for e-mails
    models/              contains model classes used in both backend and frontend
console
    config/              contains console configurations
    controllers/         contains console controllers (commands)
    migrations/          contains database migrations
    models/              contains console-specific model classes
    runtime/             contains files generated during runtime
backend
    assets/              contains application assets such as JavaScript and CSS
    config/              contains backend configurations
    controllers/         contains Web controller classes
    models/              contains backend-specific model classes
    runtime/             contains files generated during runtime
    views/               contains view files for the Web application
    web/                 contains the entry script and Web resources
frontend
    assets/              contains application assets such as JavaScript and CSS
    config/              contains frontend configurations
    controllers/         contains Web controller classes
    models/              contains frontend-specific model classes
    runtime/             contains files generated during runtime
    views/               contains view files for the Web application
    web/                 contains the entry script and Web resources
    widgets/             contains frontend widgets
vendor/                  contains dependent 3rd-party packages
environments/            contains environment-based overrides
tests                    contains various tests for the advanced application
    codeception/         contains tests developed with Codeception PHP Testing Framework
```